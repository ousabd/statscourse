
# 1. Load the tidyverse package
library(tidyverse)

# 2. Load the data
# TIPS:
# - you should provide the full path rather than the just the name of the file
# - on Windows you need to replace the slashes "\" by double slashes "\\" or back-slashes "/"
filepath <- "/media/truecrypt1/pro/teaching/2021_EcoleDoctorale_Stats/TD/TD_data.csv" ## !!! replace this by the path to the file on your computer
df <- read_tsv(filepath)

# NOTEs: Often data is stored in one file per participant, in which case we need to load many files and concatenate the data.
# Below is an example of code that does exactly that:

# listfiles <- list.files(pattern = "\\.txt$") # will list only files that end with ".txt"
# df <- data.frame()
# 
# for (f in listfiles) {
#   df.tmp <- read_delim(f, delim = ",")
#   df <- bind_rows(df, df.tmp)
# }


# 3. Calculate the average reaction time across all trials and all participants
# TIP: you can use "shift+ctrl+M" as a shortcut for the pipe (%>%)
df %>% summarise(avg_rt = mean(rt)) 

# 4. Calculate the average reaction time separately for each participant
# TIP: you can introduce line breaks in your code to improve readability
df %>% 
  group_by(subject) %>%
  summarise(avg_rt = mean(rt)) 

# 5. Calculate the average reaction time separately for each participant and each Cue condition
df %>% 
  group_by(subject,cue) %>%
  summarise(avg_rt = mean(rt)) 

# 6. Calculate the benefit of the informative cue, i.e. the difference in average response times between the two Cue conditions (separately for each participant)
df %>% 
  group_by(group,subject,cue) %>%
  summarise(avg_rt = mean(rt)) %>% 
  spread(key = cue, value = avg_rt) %>% 
  mutate(cue_effect = informative-uninformative)


# 7. Design and code a figure representing the effect of the cue and its developmental evolution across age groups

# - Prepare a dataframe for plotting
df.plot <- df %>%
  # --- Rename and transform the age group into a factor
  rename(agegroup = group) %>%
  mutate(agegroup = factor(age, levels = c("child","teenage","adult"))) %>% 
  # --- calculate average reaction time for each condition and each participant
  group_by(agegroup,subject,cue) %>%
  summarise(avg_rt = mean(rt)) %>%
  # --- calculate group-level statistics (mean and standard deviation)
  group_by(age,cue) %>%
  summarise(age_avg_rt = mean(avg_rt), age_sd_rt = sd(avg_rt))
  

# - Plot
# TIPs:
# - go ahead and try different options (for example bars instead of lines, etc.)
# - don't forget the "theme": titles, legend, colors etc.
# - play with the code below to understand its effect on the figure.
# - the ggplot2 bible can be found at https://ggplot2.tidyverse.org/reference/
ggplot(data = df.plot, mapping = aes(x = agegroup, y = age_avg_rt, ymin = age_avg_rt-age_sd_rt, ymax = age_avg_rt+age_sd_rt,
                                     color = cue, fill = cue, group = cue)) +
  geom_line() +
# geom_col(position = position_dodge()) +
# geom_errorbar(position = position_dodge(), width = 0.2) +
  expand_limits(y = 0) +
  scale_y_continuous(breaks = seq(0,800,100)) +
  scale_color_discrete() +
  labs(title = "Developmental trajectory of attentional cue effect",
       subtitle = "30 participants per age group",
       y = "Average reaction time (ms)",
       x = "Age group",
       color = "Cue") +
  theme(panel.background = element_rect(fill = "white"),
        legend.position = "bottom")


# 8. The figure indicates that there is an effect of the cue, that matures along with the developmental trajectory.
# How can we test these hypotheses formally?

# ANSWER: using a linear model, where the average reaction times is predicted by the Cue and Age group factors

# - Prepare the data for the statistical model
df.model <- df %>%
  # --- Rename and transform the age group into a factor
  rename(agegroup = group) %>%
  mutate(agegroup = factor(agegroup, levels = c("child","teenage","adult"))) %>% 
  # --- calculate average reaction time for each condition and each participant
  group_by(agegroup,subject,cue) %>%
  summarise(avg_rt = mean(rt))

# - Fit the model
m <- lm(avg_rt ~ cue*agegroup, data = df.model)
m <- lm(avg_rt ~ cue + agegroup + cue:agegroup, data = df.model) # this formula is equivalent to the previous one

# - Check the model assumptions using diagnostic plots
# TIPS: always check model assumptions BEFORE you calculate p-vaues, so you are not tempted to judge the quality
# of the assumptions based on the results
plot(m)

# Alternatively, you can get the same plots drawn by ggplot and combined altogether:
library(ggfortify) # install if you don't have it yet: install.packages("ggfortify")
autoplot(m)


# - Test your hypotheses (effect of Cue + interaction with Age group)
library(car)
Anova(m)





# BONUS EXERCICE
# ==============
# Imagine you get a Covid vaccine in a large vaccination center, open from 8am to 8pm. While you wait for the
# observation period (15 min), and out of curiosity, you count the people getting out of the vaccination boxes,
# and find 82.
#
# Questions
# 1. Can you estimate the number of people getting vaccined per day in this center?
n.min = 12*60 # number of minutes for which the center is open
n.people = 82/15*n.min # cross-multiplication
n.people

# 2. 2.	Under which conditions is your estimate correct? Would you consider these conditions fulfilled?
# If the 15 minutes of dala collection is representative of the entire day; or said otherwise, if the rhythm of
# vaccination during the 15 minutes of observation is constant across the day. This assumption is reasonable if the
# capacity of the vaccination center is the same from 8am to 8pm, and if all slots are booked.

# 3. What are is the confidence interval around your estimate?
# The population of interest are events (injections), and the parameter of interest is the rate of injections.
# This kind of population parameter is well described using Poisson distribution:
poisson.test(x = 82, T = 15)$conf.int*n.min # provides estimate and confidence interval

# 4. The objective set by the hospital for this vaccination center is 5000/day. According to your data, is the goal met?
# => The 95% confidence interval does not contain 5000, indicating that the p-value associated to the null hypothesis
#    (H0: rate = 5000/day) is below 0.05. We can verify using a formal test:
poisson.test(x = 82, T = 15, r = 5000/n.min)
# => p-value = .031, therefore we can reject the null hypothesis

# You told a friend about your little observational study, and she decides to do the same when she goes for her
# first injection, in the same center, two weeks after you. She counts 77 people in 11 minutes.

# 5. Is the center most efficient now than 2 weeks before?
poisson.test(c(82,77), c(15,11), r=1)
# => p = .13, therefore we can not reject the null hypothesis
# Note: because we now compare two samples, the argument "r" refers to the *ratio* between the two estimated rates.

# 6. Is the goal of 5000 vaccinations/day met now?
poisson.test(x = 77, T = 11, r = 5000/n.min)
# => p-value = .91, therefore we can not reject the null hypothesis that 5000 people got vaccinated in a day
# (but we can not accept it either!)