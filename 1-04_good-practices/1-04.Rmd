---
title: An R Markdown document converted from "1-04.ipynb"
output: html_document
---

```{r}
library(car) # for ANOVAs
library(pwr) # to calculate statistcal power
library(tidyverse) # for easy manipulations of dataframe and plotting with ggplot2

theme_set(theme_minimal())
```

```{r}
R.Version()$version.string
```

# Calculating statistical power

The **pwr** library provides convenient functions for the most common, simple statistical models: t-test, Chi square, Pearson correlation, one-way Aonva.

It uses the formal relationship that can be expressed for simple models between the sample size, effect size, significance level and power. It can calculate any of these 4 variables based on the others.

## Example 1 (you need always more than what you think)

How many participants do I need for a **between-group** design with **2 conditions**, with a minimum interesting **effect size** (Cohen's *d*) of **0.2**, and a **statistical power of 90%**?

```{r}
pwr.t.test(d = 0.2, power = .9, sig.level = .05, type = "two.sample")
```

That's a lot! Let's be less ambitious. A statistical power of **80%**?

```{r}
pwr.t.test(d = 0.2, power = .8, sig.level = .05, type = "two.sample")
```

Still a lot... What if I use a **within-subject** design then?

```{r}
pwr.t.test(d = 0.2, power = .8, sig.level = .05, type = "paired")
```

## Example 2 (keep it simple)

How many participants do I need for a **between-subject** design with **4 conditions**, with an estimated **effect size** (Cohen's *f*) of **0.25**, and a **statistical power of 80%**?

```{r}
pwr.anova.test(k = 4, f = 0.25, power = .8, sig.level = .05)
```

```{r}
pwr.anova.test(k = 2, f = 0.2, power = .8, sig.level = .05)
# pwr.t.test(d = 0.5, power = .8, sig.level = .05)
```

## Example 2 (post hoc power - never do that!)

```{r}
pwr.r.test(r = 0.48, n = 27, sig.level = .05)
```
## EXERCISE

Calculate the statistical power of your own study, both for the expected effect size (e.g., based on the literature), and for a minimal interesting effect size.
Is your study properly powered?



# Consequences of low statistical power

```{r}
# Let's write a function that simulates 2 groups of normally distributed data with a certain effect size. It will be useful later for batch simulations.

simulate = function(n, es = 0.5, alpha = .05) {
    
    yA = rnorm(n, mean = es, sd = 1)
    yB = rnorm(n, mean = 0, sd = 1)
    
    p = t.test(yA, yB, paired = F)$p.value
    d = (mean(yA) - mean(yB)) / 1
    
    return(c(p,d))
}
```

## Type M and S errors at low power

```{r}
# Theoretical power for a minimal interesting effect size
pwr.t.test(n = 30, d = 0.1, sig.level = .05)
```

```{r}
# Power estimated from simulations

N = 10000
es = 0.1

# Initialize the vectors that will store the results from the N simulated experiments
p = vector("numeric",N)
d = vector("numeric",N)

for (k in 1:N) {
    # Calculate the p-value and observed effect size from one simulated experiment
    sim = simulate(n = 30, es = es, alpha = .05)
    # Store results
    p[k] = sim[1]
    d[k] = sim[2]
}

# Power estimated from simulations (compare to the theoretical value above)
sum(p < .05)/N 

# Effect size estimated from simulations (compare to the theoretical effect size "es")
mean(d) 
```

```{r}
### Visualize simulations

# - arrange results in a dataframe
df.plot <- data.frame(n = seq(N), p = p, d = d) 

# - viualize the distribution of observed effect sizes
ggplot(df.plot, aes(x = d)) +
  geom_histogram(fill = "grey70") +
  geom_vline(xintercept = es) +
  annotate(geom = "text", label = " true effect size", x = es, y = Inf, hjust = 0, vjust = 1.5) 

# - viualize the distribution of observed effect sizes as a function of test significance
ggplot(df.plot, aes(x = d, fill = (p<.05))) +
  geom_histogram(position = "identity", alpha = 0.5) +
  geom_vline(xintercept = es) +
  annotate(geom = "text", label = " true effect size", x = es, y = Inf, hjust = 0, vjust = 1.5) +
  scale_fill_hue(labels = c(`TRUE`="p<.05",`FALSE`="p>.05")) +
  guides(fill = guide_legend(title = "Statistical significance", override.aes = list (alpha = 1)))

### CONCLUSION
# WHen the statistical power is low, significant studies can geatly overestimate effect sizes, and can even get the sign of the effect wrong! Therefore publication bias (the fact that studies are more likely to get published if they have significant results) leads to an overestimation of effect sizes.
```

```{r}
# Type M error: ratio of effect size of statistically significant simulated experiments / true effet size

mean(abs(d[p<.05]))/es # 1 = no magnitude amplification
```

```{r}
# Type S error: proportion of statistically significant simulated experiments that get the sign of the effect wrong

sum((p<.05) & (d<0))/sum(p<.05) # !!!
```

## Type M and S errors as a function of power

How are type M and S errors affected by statistical power? Let's re-run the entire simulation and analysis pocedure, but for a whole range of statistical power values. We will manipulate power by exploring a range of true effect sizes while keeping the sample size constant (but we could as well do the opposite). 

```{r}

# Range of true effect sizes
listES = seq(from = 0.1, to = 1, by = 0.02)

# Initialize the dataframe that will contain the results
df = data.frame(es = listES, power = NA, typeM = NA, typeS = NA)

N = 1000 # we decrease the number of simulations to speed up the calculations

for (es in listES) {
    
    p = vector("numeric",N)
    d = vector("numeric",N)
    for (k in 1:N) {
        sim = simulate(n = 30, es = es, alpha = .05)
        p[k] = sim[1]
        d[k] = sim[2]
    }
    
    # True power
    df[df$es==es,"power"] = pwr.t.test(n = 30, d = es, sig.level = .05)$power
    
    # Type M error
    df[df$es==es,"typeM"] = mean(abs(d[p<.05]))/es
    
    # Type S error
    df[df$es==es,"typeS"] = sum((p<.05) & (d<0))/sum(p<.05)
 
}
```

Let's look at the results.

```{r, fig.width=3, fig.height=3}
ggplot(df, aes(y = typeM, x = power, group = )) +
  geom_hline(yintercept = 1, linetype = 2, color = "grey50") +
  geom_line() +
  expand_limits(x = 0, y = 0) +
  scale_x_continuous(limits = c(0,1), breaks = scales::pretty_breaks(5), label = scales::percent) +
  labs(title = "Type M error",
       x = "Statistical power",
       y = "Magnitude amplitfication")

ggplot(df, aes(y = typeS, x = power, group = )) +
  geom_line() +
  scale_x_continuous(breaks = seq(.1,1.5,.1)) +
  labs(title = "Type S error", y = "Fraction of simulations getting the sign wrong")
```

*CONCLUSION:*
While type S error occurs virtualy only in very low powered studies (power < 20%), effect sizes are overestimated in all studies with a power <90%, and it becomes a serious concern below 60-70%.

## Post hoc (observed) power as a function of true power

Can we estimate the statistical power of a study based on its results (i.e., the reported effect size)? Let's use the data we have just simulated.

```{r}
# Calculate the "observed" aka "post hoc" statistical power
df.pwr.posthoc <- df %>% mutate(power_sim = pwr.t.test(n = 30, d = typeM*es, sig.level = .05)$power)

# Plot
ggplot(df.pwr.posthoc, aes(y = power_sim, x = power)) +
geom_point() +
geom_abline(slope = 1, linetype = 2, color = "grey50") +
annotate(geom = "text", label = "expected relationship", x = .70, y = .73, angle = 45, size = 3.5, color = "grey50") + 
scale_x_continuous(breaks = scales::pretty_breaks(8), label = scales::label_percent(accuracy = 1)) +
scale_y_continuous(breaks = scales::pretty_breaks(5), label = scales::label_percent()) +
coord_fixed() +
labs(x = "True power", y = "Post hoc (observed) power")
```

As expected, post hoc power overestimates true power (on average).
It looks like there is a rather straightforward, close to linear relationship between post hoc power and true power, suggesting we could infer (even approximately) the latter from the former. However, remember that we are plotting the average over many simulations. Actually, the variability is shockingly wide as demonstrated by Daniel Lakens (http://daniellakens.blogspot.com/2014/12/observed-power-and-what-to-do-if-your.html): observed power depends so much on the data that it can take pretty much any value, and is largely independent from true power.

## EXERCISE

For 3 different levels of statistical power (20%, 50% and 80%) simulate many experiments and visualize the distribution of post hoc statistical power.

Tip: use boxplots or violin plots.



# Consequences of QRPs

## Simulation of situation C from Simmons, Nelson & Simonsohn 2012

The following situation reproduces **situation C** in Simmons, Nelson & Simonsohn 2012 (table 1, see also slides 28 and 29 in the course):
We model simulated data with or without a covariate, with or without interaction with the covariate, and we choose the "most significant" one. Remember that there are no effects whatsoever in the simulated data, and therefore any statistically significant outcome is a false positive.

```{r}
### Function that does step 1 (generate random data with no effect) and step 2 (exploit the analytic degrees-of-freedom and retain the best p-value)

simulate_qrpC = function(n = 30, alpha = .05) {
  
    # STEP 1
    y0 = rnorm(n)
    yT = rnorm(n)
    
    # We use a random binary variable as a covariate
    cov = as.integer(rbernoulli(n))
    # We could also use a uniform variable 
    # cov = runif(n, min = 18, max = 70)
    
    df = data.frame(id = seq(n), baseline = y0, treatment = yT, covariate = cov) %>%
        gather(key = condition, value = y, baseline, treatment)
    
    
    # STEP 2: calculate and store p-values from all possible modeling choices
    # Model without covariate
    p = t.test(y0,yT, paired = F)$p.value
    
    # Model with covariate
    m_cov = lm(y ~ covariate + condition, df)
    p_cov = Anova(m_cov)["condition","Pr(>F)"]
    
    # Model with covariate interacting with the experimental condition
    m_interact = lm(y ~ condition*covariate, df)
    # - Main effect of CONDITION
    p_interact1 = Anova(m_interact)["condition","Pr(>F)"]
    # - Interaction
    p_interact2 = Anova(m_interact)["condition:covariate","Pr(>F)"]
    
    return(c(p,p_cov,p_interact1,p_interact2))
}
```

```{r}
### Repeat steps 1 and 2 many times

N = 1000

p = vector("numeric",N)
p_qrp = vector("numeric",N)
for (k in 1:N) {
    p_sim = simulate_qrpC()
    p[k] = p_sim[1]
    p_qrp[k] = min(p_sim)
}
```

```{r}
### Estimate false positive rates based on simulations

sum(p < .05)/N # this one should be close to .05
sum(p_qrp < .05)/N # this one gives an idea of how much the QRP inflates the false positive rate
```

## EXERCISE

Write the code to simulate the other situations from Simmons, Nelson & Simonsohn 2012. Compare your results to theirs.

